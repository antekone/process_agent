use std::path::PathBuf;
use std::io::BufReader;
use std::fs::File;
use maps;

fn get_reader(filename: &'static str) -> BufReader<File> {
    let mut d = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    d.push("test");
    d.push(filename);

    let f = File::open(d).unwrap();
    BufReader::new(f)
}

#[test]
fn maps_reader_just_parse() {
    let result = maps::parse_maps(&mut get_reader("maps1"));
    println!("{:?}", result);
    assert!(result.is_ok());
}

#[test]
fn maps_reader_ret_bigger_than_0() {
    let result = maps::parse_maps(&mut get_reader("maps1"));
    assert!(!result.unwrap().is_empty());
}

#[test]
fn maps_reader_check_number_of_items() {
    let result = maps::parse_maps(&mut get_reader("maps1")).unwrap();
    assert!(result.len() == 27);
}

#[test]
fn maps_reader_check_1st_record() {
    let result = maps::parse_maps(&mut get_reader("maps1")).unwrap();
    let rec = &result[0];
    assert!(rec.region_begin == 0x5559a1f71000);
    assert!(rec.region_end == 0x5559a1f8d000);
    assert!(rec.prot.readable == true);
    assert!(rec.prot.writable == false);
    assert!(rec.prot.executable == true);
    assert!(rec.title == "/usr/bin/cp");
}

#[test]
fn maps_reader_check_9th_record() {
    let result = maps::parse_maps(&mut get_reader("maps1")).unwrap();
    let rec = &result[9];
    assert!(rec.region_begin == 0x7f2b6c1ff000);
    assert!(rec.region_end == 0x7f2b6c203000);
    assert!(rec.prot.readable == true);
    assert!(rec.prot.writable == true);
    assert!(rec.prot.executable == false);
    assert!(rec.title == "");
}

#[test]
fn maps_reader_check_last_record() {
    let result = maps::parse_maps(&mut get_reader("maps1")).unwrap();
    let rec = &result.last().unwrap();
    assert!(rec.region_begin == 0x7ffe4a53c000);
    assert!(rec.region_end == 0x7ffe4a53e000);
    assert!(rec.prot.readable == true);
    assert!(rec.prot.writable == false);
    assert!(rec.prot.executable == true);
    assert!(rec.title == "[vdso]");
}

