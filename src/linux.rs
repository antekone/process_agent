#![allow(unused)]
use ProcessEnumerator;
use std::fs::{self, File};
use std::io::{BufReader, BufRead, Read, Seek, SeekFrom};
use std::path::Path;
use std::error::Error;
use super::{ProcessEntry, MemoryRegion};
use maps;

pub struct LinuxProcessEnumerator {
    dummy: u32,
    mem: Option<File>
}

fn is_numeric_name(name: &str) -> bool {
    name.chars()
        .filter(|pc| {
            let c = *pc;
            c < '0' || c > '9'
        })
        .peekable()
        .peek()
        .is_none()
}

fn is_process_entry(p: &Path) -> bool {
    let name = p.file_name().unwrap().to_str().unwrap();
    p.is_dir() && is_numeric_name(name)
}

fn read_cmdline_string(pid: u32) -> Option<String> {
    let path_str = format!("/proc/{}/cmdline", pid);
    let path = Path::new(path_str.as_str());

    match fs::File::open(path) {
        Ok(ref mut fp) => {
            let mut buf = String::new();
            if let Err(_) = fp.read_to_string(&mut buf) {
                return None;
            }

            Some(buf.chars()
                 .map(|c| if c == 0 as char { ' ' } else { c })
                 .collect::<String>()
                 .trim()
                 .to_string())
        },

        Err(_) => None
    }
}

fn read_exe_path(pid: u32) -> Option<String> {
    let path_str = format!("/proc/{}/exe", pid);
    let path = Path::new(path_str.as_str());

    match fs::read_link(path) {
        Ok(new_path) => Some(new_path.as_path().to_str().unwrap().to_string()),
        Err(_) => None
    }
}

fn read_title(pid: u32) -> Option<String> {
    let path_str = format!("/proc/{}/status", pid);
    let path = Path::new(path_str.as_str());

    let mut reader = BufReader::new(fs::File::open(path).unwrap());
    let mut line = String::new();

    reader.read_line(&mut line).unwrap();

    // skip(5) = "Name:", I hope this kernel ABI is stable :D
    Some(line.chars().skip(5).collect::<String>().trim().to_string())
}

fn list_processes() -> Result<Vec<ProcessEntry>, String> {
    debug!("starting process enumeration");

    let paths = fs::read_dir("/proc").unwrap();
    let mut ret = Vec::<ProcessEntry>::new();
    for maybe_path in paths {
        let pathbuf = maybe_path.unwrap().path();
        let path = pathbuf.as_path();

        if is_process_entry(&path) {
            let pid = u32::from_str_radix(path.file_name().unwrap().to_str().unwrap(), 10).unwrap();
            ret.push(ProcessEntry {
                pid: pid,
                command_line: match read_cmdline_string(pid) {
                    Some(s) => s,
                    None => "<no access>".to_string(),
                },
                exe_path: match read_exe_path(pid) {
                    Some(s) => s,
                    None => "<no access>".to_string(),
                },
                title: match read_title(pid) {
                    Some(s) => s,
                    None => "<no access>".to_string(),
                }
            });
        }
    }

    if ret.is_empty() {
        Err("internal error when enumerating processes".to_string())
    } else {
        Ok(ret)
    }
}

fn mem_map(_ctx: &LinuxProcessEnumerator, pid: u32) -> Result<Vec<MemoryRegion>, String> {
    let map_str = format!("/proc/{}/maps", pid);
    let path = Path::new(map_str.as_str());
    let f = match fs::File::open(path) {
        Ok(fp) => fp,
        Err(_) => return Err("no access".to_string())
    };

    let maps = match maps::parse_maps(&mut BufReader::new(f)) {
        Ok(maps) => maps,
        Err(str_) => return Err(str_.to_string())
    };

    Ok(maps)
}

fn begin_read(ctx: &mut LinuxProcessEnumerator, pid: u32) -> Result<(), String> {
    ctx.mem = Some(match File::open(format!("/proc/{}/mem", pid)) {
        Ok(f) => f,
        Err(msg) => return Err(format!("{:?}", msg)),
    });

    Ok(())
}

fn end_read(ctx: &mut LinuxProcessEnumerator) -> Result<(), String> {
    if let Some(_) = ctx.mem {
        ctx.mem = None;
    }

    Ok(())
}

fn do_read_on(f: &mut File, offset: u64, buf: &mut [u8]) -> Result<usize, String> {
    match f.seek(SeekFrom::Start(offset)) {
        Ok(new_offs) => {
            match f.read(buf) {
                Ok(count) => Ok(count),
                Err(msg) => Err(msg.description().to_string())
            }
        },
        Err(msg) => Err(msg.description().to_string())
    }
}

fn do_read(ctx: &mut LinuxProcessEnumerator, offset: u64, buf: &mut [u8]) -> Result<usize, String> {
    if let Some(ref mut f) = ctx.mem {
        do_read_on(f, offset, buf)
    } else {
        Err("read not initialized".to_string())
    }
}

impl ProcessEnumerator for LinuxProcessEnumerator {
    fn name(&self) -> &'static str { "linux" }

    fn list(&self) -> Result<Vec<ProcessEntry>, String> {
        list_processes()
    }

    fn mem_map(&self, pid: u32) -> Result<Vec<MemoryRegion>, String> {
        mem_map(self, pid)
    }

    fn begin_read(&mut self, pid: u32) -> Result<(), String> {
        begin_read(self, pid)
    }

    fn end_read(&mut self) -> Result<(), String> {
        end_read(self)
    }

    fn read(&mut self, offset: u64, buf: &mut [u8]) -> Result<usize, String> {
        do_read(self, offset, buf)
    }
}

pub fn create_process_enumerator() -> Box<ProcessEnumerator + Send + Sync> {
    Box::new(LinuxProcessEnumerator {
        dummy: 0,
        mem: None
    })
}
