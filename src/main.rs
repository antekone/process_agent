#[macro_use] extern crate cfg_if;
#[macro_use] extern crate enum_primitive;
#[macro_use] extern crate log;
extern crate log4rs;
extern crate byteorder;
extern crate grpc;
extern crate protobuf;
extern crate tls_api;
extern crate grpc_protobuf;

use std::time;
use std::env;
use std::fmt;
use log::{Record, LevelFilter};
use log4rs::append::file::FileAppender;
use log4rs::encode::pattern::PatternEncoder;
use log4rs::config::{Appender, Config, Root};
use log4rs::filter::{Filter, Response};
use std::thread;
use std::str::FromStr;
use std::process::exit;
use std::sync::{Arc, RwLock};

use protobuf::SingularPtrField;
use packets::Error;
use grpc::{RequestOptions, SingleResponse, ServerHandlerContext, ServerRequestSingle, ServerResponseUnarySink};

mod maps;
mod cmd;
mod packets;
mod packets_grpc;

cfg_if! {
    if #[cfg(unix)] {
        pub use linux::create_process_enumerator;
        mod linux;
    } else if #[cfg(windows)] {
        extern crate winapi;
        pub use windows::create_process_enumerator;
        mod windows;
    }
}

#[cfg(test)]
mod test_maps;

pub struct ProcessEntry {
    pid: u32,
    command_line: String,
    title: String,
    exe_path: String,
}

pub trait ProcessEnumerator {
    fn name(&self) -> &'static str;
    fn list(&self) -> Result<Vec<ProcessEntry>, String>;
    fn mem_map(&self, pid: u32) -> Result<Vec<MemoryRegion>, String>;
    fn begin_read(&mut self, pid: u32) -> Result<(), String>;
    fn end_read(&mut self) -> Result<(), String>;
    fn read(&mut self, offset: u64, buf: &mut [u8]) -> Result<usize, String>;
}

#[derive(Debug)]
pub struct ProtectionFlags {
    readable: bool,
    writable: bool,
    executable: bool
}

pub struct MemoryRegion {
    region_begin: u64,
    region_end:   u64,
    prot:         ProtectionFlags,
    title:        String,
}

impl std::fmt::Debug for MemoryRegion {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "MemoryRegion {{ region_begin: 0x{:016X}, region_end: 0x{:016X}, prot: {:?}, title: {:?} }}",
            self.region_begin,
            self.region_end,
            self.prot,
            self.title)
    }
}

cfg_if! {
    if #[cfg(debug_assertions)] {
        #[derive(Debug)]
        struct LogBlacklist {}

        impl LogBlacklist {
            fn new() -> Box<LogBlacklist> {
                Box::new(LogBlacklist {
                })
            }
        }

        impl Filter for LogBlacklist {
            fn filter(&self, record: &Record) -> Response {
                if record.target() == "process_agent" {
                    Response::Accept
                } else {
                    Response::Reject
                }
            }
        }

        fn init_log() {
            let logfile = FileAppender::builder()
                .encoder(Box::new(PatternEncoder::new("{l} [{M}] {m}\n")))
                .build("debug.log").unwrap();

            let config = Config::builder()
                .appender(Appender::builder().filter(LogBlacklist::new()).build("logfile", Box::new(logfile)))
                .build(Root::builder()
                           .appender("logfile")
                           .build(LevelFilter::Debug)).unwrap();

            log4rs::init_config(config).unwrap();
        }
    } else {
        fn init_log() { }
    }
}

fn process_enumerator() -> Box<ProcessEnumerator + Send + Sync> {
    create_process_enumerator()
}

struct LastMsgMonitor {
    last_msg: time::Instant
}

impl LastMsgMonitor {
    fn new() -> LastMsgMonitor {
        LastMsgMonitor {
            last_msg: time::Instant::now()
        }
    }

    fn update(&mut self) {
        self.last_msg = time::Instant::now();
    }

    fn get(&self) -> time::Instant {
        self.last_msg
    }
}

struct MainLoopController {
    graceful_exit: bool,
}

impl MainLoopController {
    fn new() -> MainLoopController {
        MainLoopController {
            graceful_exit: false,
        }
    }
}

struct ServerState {
    crumb: String,
    authenticated: bool,
    selected_pid: Option<u32>,
    pe: Option<Box<ProcessEnumerator + Send + Sync>>,
    buf_cache: Arc<RwLock<BufCache>>,

    // States shared with main loop below need additional locking guards and
    // async ref counters.
    last_msg: Arc<RwLock<LastMsgMonitor>>,
    loop_ctrl: Arc<RwLock<MainLoopController>>,
}

struct BufCache {
    buf: Vec<u8>,
}

impl BufCache {
    fn new() -> BufCache {
        BufCache {
            buf: Vec::new()
        }
    }
}

struct ProcessAgentRPCImpl {
    state: RwLock<ServerState>,
}

fn craft_error<S: AsRef<str>>(code: i32, msg: S) -> Error {
    let mut err = Error::new();
    err.code = code;
    err.msg = msg.as_ref().to_string();
    err
}

fn error_field<S: AsRef<str>>(code: i32, msg: S) -> SingularPtrField<Error> {
    error!("crafting error: code={}, msg={}", code, msg.as_ref());
    SingularPtrField::some(craft_error(code, msg))
}

impl ProcessAgentRPCImpl {
    fn check_auth(&self) -> bool {
        let state = self.state.read().unwrap();
        state.authenticated
    }

    /// Returns true if the agent is in authenticated and not locked states.
    /// Returns false if the agent is NOT authenticated OR is locked.
    fn not_locked(&self) -> bool {
        let state = self.state.read().unwrap();
        state.authenticated && state.selected_pid == None
    }

    /// Returns true if the agent is in authenticated and locked states.
    /// Returns false if the agent is NOT authenticated OR is NOT locked.
    fn locked(&self) -> bool {
        let state = self.state.read().unwrap();
        state.authenticated && state.selected_pid != None
    }

    fn last_msg_update(&self) {
        let state = self.state.write().unwrap();
        state.last_msg.write().unwrap().update();
    }
}

impl packets_grpc::ProcessAgentRPC for ProcessAgentRPCImpl {
    fn leave(&self, _: ServerHandlerContext, _req: ServerRequestSingle<packets::LeaveRequest>, resp: ServerResponseUnarySink<packets::LeaveReply>) -> ::grpc::Result<()> {
        let mut r = packets::LeaveReply::new();
        debug!("--- Entering: leave");

        self.last_msg_update();
        if !self.check_auth() {
            r.error = error_field(1, "bad initial state for ping call");
        } else {
            debug!("requesting main loop break");
            let state = self.state.read().unwrap();
            let mut loop_ctrl = state.loop_ctrl.write().unwrap();
            loop_ctrl.graceful_exit = true;
        }

        resp.finish(r)
    }

    fn auth(&self, _: ServerHandlerContext, req: ServerRequestSingle<packets::AuthRequest>, resp: ServerResponseUnarySink<packets::AuthReply>) -> ::grpc::Result<()> {
        let mut r = packets::AuthReply::new();
        let crumb_valid;

        debug!("--- Entering: auth");

        self.last_msg_update();

        {
            let state_ro = self.state.read().unwrap();
            if state_ro.authenticated {
                debug!("already authenticated");
                r.error = error_field(1, "re-authentication not allowed");
                return resp.finish(r);
            }

            crumb_valid = req.message.crumb == state_ro.crumb;
        }

        r.banner = format!("{} {}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"));
        r.version = 1;

        debug!("auth: crumb_valid={}", crumb_valid);

        if !crumb_valid {
            r.error = error_field(1, "invalid auth crumb");
        } else {
            let mut state_rw = self.state.write().unwrap();
            state_rw.authenticated = true;
        }

        resp.finish(r)
    }

    fn ping(&self, _: ServerHandlerContext, req: ServerRequestSingle<packets::PingRequest>, resp: ServerResponseUnarySink<packets::PingReply>) -> ::grpc::Result<()> {
        let mut r = packets::PingReply::new();
        debug!("--- Entering: ping");

        self.last_msg_update();

        // Require authentication and don't care about the pid lock.
        if !self.check_auth() {
            r.error = error_field(1, "bad initial state for ping call");
        } else {
            r.value = req.message.value + 1;
        }

        resp.finish(r)
    }

    fn proclist(&self, _: ServerHandlerContext, _req: ServerRequestSingle<packets::ProcListRequest>, resp: ServerResponseUnarySink<packets::ProcListReply>) -> ::grpc::Result<()> {
        let mut r = packets::ProcListReply::new();
        debug!("--- Entering: proclist");

        self.last_msg_update();

        // Don't allow process enumeration if we're in locked mode.
        if !self.not_locked() {
            r.error = error_field(1, "bad initial state for proclist call");
            return resp.finish(r);
        }

        let pe = process_enumerator();
        r.engine = pe.name().to_string();

        let lst = match pe.list() {
            Ok(l) => l,
            Err(msg) => {
                r.error = error_field(1, format!("proclist error: {}", msg));
                return resp.finish(r);
            }
        };

        let mut proc_list: Vec<packets::process_entry> = Vec::new();
        for proc_item in lst {
            let mut me = packets::process_entry::new();
            me.pid = proc_item.pid;
            me.command_line = proc_item.command_line;
            me.title = proc_item.title;
            me.exe_path = proc_item.exe_path;
            proc_list.push(me);
        }

        r.process = protobuf::RepeatedField::from_vec(proc_list);
        debug!("engine {} returned {} processes", r.engine, r.process.len());
        resp.finish(r)
    }

    fn procsel(&self, _: ServerHandlerContext, mut request: ServerRequestSingle<packets::ProcSelRequest>, resp: ServerResponseUnarySink<packets::ProcSelReply>) -> ::grpc::Result<()> {
        let mut r = packets::ProcSelReply::new();
        let req = request.take_message();
        debug!("--- Entering: procsel");

        self.last_msg_update();

        // Require authentication and don't allow re-selection after pid
        // lock is active.
        if !self.not_locked() {
            r.error = error_field(1, "bad initial state for procsel call");
            return resp.finish(r);
        }

        let mut state = self.state.write().unwrap();

        // Lock this instance of process_agent to just one pid.
        state.selected_pid = Some(req.pid);

        // Reading other pids from this instance from now on should be
        // impossible. A new instance of process_agent is needed in
        // such case.

        resp.finish(r)
    }

    fn procmap(&self, _: ServerHandlerContext, _req: ServerRequestSingle<packets::ProcMapRequest>, resp: ServerResponseUnarySink<packets::ProcMapReply>) -> ::grpc::Result<()> {
        let mut r = packets::ProcMapReply::new();
        debug!("--- Entering: procmap");

        self.last_msg_update();

        // Require authentication and a pid lock.
        if !self.locked() {
            r.error = error_field(1, "bad initial state for procmap call");
            return resp.finish(r);
        }

        let pid = { self.state.read().unwrap().selected_pid.unwrap() };
        debug!("starting to enumerate memory regions for pid {}", pid);

        let pe = process_enumerator();
        let maps = match pe.mem_map(pid) {
            Ok(v) => v,
            Err(msg) => {
                debug!("Error when enumerating memory map: {}", msg);
                r.error = error_field(1, msg);
                return resp.finish(r);
            }
        };

        let mut map_list = Vec::<packets::memory_region>::new();
        for map_item in maps {
            let mut mr = packets::memory_region::new();
            mr.region_begin = map_item.region_begin;
            mr.region_end = map_item.region_end;
            mr.flag_r = map_item.prot.readable;
            mr.flag_w = map_item.prot.writable;
            mr.flag_x = map_item.prot.executable;
            mr.title = map_item.title;
            map_list.push(mr);
        }

        r.region = protobuf::RepeatedField::from_vec(map_list);
        resp.finish(r)
    }

    fn beginread(&self, _: ServerHandlerContext, _req: ServerRequestSingle<packets::BeginReadRequest>, resp: ServerResponseUnarySink<packets::BeginReadReply>) -> ::grpc::Result<()> {
        let mut r = packets::BeginReadReply::new();
        debug!("--- Entering: beginread");

        if !self.locked() {
            r.error = error_field(1, "bad initial state for beginread call");
            return resp.finish(r);
        }

        let mut state_rw = self.state.write().unwrap();
        let mut pe = Some(process_enumerator());
        let pid = state_rw.selected_pid.unwrap();

        match pe {
            Some(ref mut pe) => match pe.begin_read(pid) {
                Ok(_) => (),
                Err(msg) => {
                    r.error = error_field(1, msg);
                    return resp.finish(r);
                }
            },
            None => ()
        };

        state_rw.pe = pe;
        resp.finish(r)
    }

    fn endread(&self, _: ServerHandlerContext, _req: ServerRequestSingle<packets::EndReadRequest>, resp: ServerResponseUnarySink<packets::EndReadReply>) -> ::grpc::Result<()> {
        let mut r = packets::EndReadReply::new();
        debug!("--- Entering: endread");

        if !self.locked() {
            r.error = error_field(1, "bad initial state for endread call");
            return resp.finish(r);
        }

        let mut state_rw = self.state.write().unwrap();
        match state_rw.pe {
            Some(_) => state_rw.pe = None,
            None => {
                r.error = error_field(1, "nothing to end");
                return resp.finish(r);
            }
        }

        resp.finish(r)
    }

    fn procread(&self, _: ServerHandlerContext, mut request: ServerRequestSingle<packets::ProcReadRequest>, resp: ServerResponseUnarySink<packets::ProcReadReply>) -> ::grpc::Result<()> {
        let mut r = packets::ProcReadReply::new();
        let req = request.take_message();
        debug!("--- Entering: procread");

        if !self.locked() {
            r.error = error_field(1, "bad initial state for procread call");
            return resp.finish(r);
        }

        let mut state_rw = self.state.write().unwrap();
        let offset = req.offset;
        let req_size = req.size as usize;

        if req_size > 128 * 1024 {
            r.error = error_field(1, "size is too big");
            return resp.finish(r);
        }

        let buf_cache = state_rw.buf_cache.clone();
        let mut buf_obj = buf_cache.write().unwrap();
        let buf = &mut buf_obj.buf;

        if buf.len() < req_size {
            buf.resize(req_size, 0);
        }

        if let Some(ref mut pe) = state_rw.pe {
            let mut slice = &mut buf[0 .. req_size];
            let actually_read = match pe.read(offset, &mut slice) {
                Ok(r) => r,
                Err(msg) => {
                    r.error = error_field(1, msg);
                    return resp.finish(r);
                }
            };

            r.buffer = slice[0 .. actually_read].to_vec();
        } else {
            r.error = error_field(1, "read session has not been started yet");
            return resp.finish(r);
        }

        resp.finish(r)
    }
}

fn main() {
    init_log();
    debug!("Starting process_agent");

    let port_str = match env::var("__RPC_PORT") {
        Ok(s) => s.to_string(),
        Err(_) => {
            error!("No port information, failing agent");
            exit(1);
        }
    };

    // TODO: overwrite this environment variable without reallocating memory

    let crumb_str = match env::var("__RPC_CRUMB") {
        Ok(s) => s.to_string(),
        Err(_) => {
            error!("No crumb information, failing agent");
            exit(2);
        }
    };

    debug!("[main] Listening on port '{}'", port_str);

    let port = u16::from_str(port_str.as_str()).unwrap();
    let mut server = grpc::ServerBuilder::new_plain();
    server.http.set_port(port);

    let last_msg = Arc::new(RwLock::new(LastMsgMonitor::new()));
    let loop_ctrl = Arc::new(RwLock::new(MainLoopController::new()));

    let shared_state = RwLock::new(ServerState {
        crumb: crumb_str,
        authenticated: false,
        selected_pid: None,
        pe: None,
        buf_cache: Arc::new(RwLock::new(BufCache::new())),
        last_msg: Arc::clone(&last_msg),
        loop_ctrl: Arc::clone(&loop_ctrl),
    });

    server.add_service(packets_grpc::ProcessAgentRPCServer::new_service_def(ProcessAgentRPCImpl {
        state: shared_state,
    }));

    let _server = server.build().expect("server.build");
    const TIMEOUT: u64 = 15;

    loop {
        thread::sleep(time::Duration::from_millis(333));

        let time_since_last_request = time::Instant::now().duration_since({
            last_msg.read().unwrap().get()
        });

        let graceful_exit = { loop_ctrl.read().unwrap().graceful_exit };
        if graceful_exit {
            debug!("[main] Requested graceful_exit, so quitting rpc server");
            break;
        }

        if time_since_last_request > time::Duration::from_secs(TIMEOUT) {
            info!("[main] No request in last {} seconds, quitting rpc server", TIMEOUT);
            break;
        }
    }

    info!("[main] RPC server ends");
}
