#![allow(unused)]
use enum_primitive::FromPrimitive;
use std::io::{Read, Write};
use byteorder::{ReadBytesExt, WriteBytesExt, LittleEndian};
use super::{MemoryRegion};

#[repr(C)]
struct Cmd {
    packed: u32,
}

pub struct Context {
    pid: u32,
    p_enum: Box<super::ProcessEnumerator>,
}

impl Context {
    pub fn new() -> Context {
        Context {
            pid: 0,
            p_enum: super::process_enumerator(),
        }
    }
}

enum_from_primitive! {
    #[derive(Copy, Clone, Debug)]
    pub enum CmdType {
        Ping = 0,
        Exit = 1,
        ListProcesses = 2,
        SelectProcess = 3,
    }
}

impl Cmd {
    fn get_size(&self) -> u32 {
        (self.packed & 0xFFFFFF) >> 4
    }

    fn get_cmd(&self) -> CmdType {
        CmdType::from_u32(self.packed & 0xFF).unwrap()
    }
}

pub fn step(reader: &mut Read, writer: &mut Write, ctx: &mut Context) -> bool {
    debug!("entering step()");

    let ret = match make(reader) {
        None => return false,
        Some(c) => invoke(c.get_cmd(), reader, writer, ctx),
    };

    writer.flush().unwrap();
    ret
}

fn make(reader: &mut Read) -> Option<Cmd> {
    Some(Cmd {
        packed: match reader.read_u32::<LittleEndian>() {
            Err(_) => return None,
            Ok(v) => {
                debug!("Received packed command u32: 0x{:08X}", v);
                v
            }
        }
    })
}

fn invoke(cmd: CmdType, reader: &mut Read, writer: &mut Write, ctx: &mut Context) -> bool {
    debug!("Entering invoke() with cmd={:?}", cmd);
    match cmd {
        CmdType::Ping => {
            handler_ping(writer);
            true
        },
        CmdType::Exit => {
            handler_exit(writer);
            false
        },
        CmdType::ListProcesses => {
            handler_list_processes(writer, ctx);
            true
        },
        CmdType::SelectProcess => {
            handler_select_process(reader, writer, ctx);
            true
        },
    }
}

fn write_u32(writer: &mut Write, value: u32) {
    writer.write_u32::<LittleEndian>(value).unwrap();
}

fn write_u64(writer: &mut Write, value: u64) {
    writer.write_u64::<LittleEndian>(value).unwrap();
}

fn write_u8(writer: &mut Write, ch: u8) {
    writer.write_u8(ch as u8).unwrap();
}

fn read_u32(reader: &mut Read) -> u32 {
    reader.read_u32::<LittleEndian>().unwrap()
}

fn write_string(writer: &mut Write, string: &str) {
    write_u32(writer, string.len() as u32);
    writer.write(string.as_bytes()).unwrap();
}

fn handler_ping(writer: &mut Write) {
    debug!("entering handler_ping");

    debug!("writing pong");
    write_u32(writer, 0xcafebabe);
}

fn handler_exit(_writer: &mut Write) {
    debug!("entering handler_exit");
}

fn handler_list_processes(writer: &mut Write, ctx: &mut Context) {
    debug!("entering handler_list_processes");

    debug!("created process enumerator: {:?}", ctx.p_enum.name());

    let lst = match ctx.p_enum.list() {
        Err(str) => {
            debug!("process enumerator returned error during list(): {:?}", str);
            write_u32(writer, 0);
            write_string(writer, str.as_str());
            return;
        },
        Ok(lst) => lst
    };

    write_u32(writer, lst.len() as u32);
    for item in lst {
        write_u32(writer, item.pid);
        write_string(writer, item.command_line.as_str());
        write_string(writer, item.exe_path.as_str());
        write_string(writer, item.title.as_str());
    }
}

macro_rules! write_u8_prot {
    ($writer: expr, $value: expr, $yes: expr, $no: expr) => (
        write_u8($writer, match $value {
            true => $yes as u8,
            false => $no as u8,
        });
    )
}

fn dump_maps(writer: &mut Write, maps: &Vec<MemoryRegion>) {
    write_u32(writer, maps.len() as u32);
    for entry in maps {
        write_u64(writer, entry.region_begin);
        write_u64(writer, entry.region_end);

        write_u8_prot!(writer, entry.prot.readable, 'r', '-');
        write_u8_prot!(writer, entry.prot.writable, 'w', '-');
        write_u8_prot!(writer, entry.prot.executable, 'x', '-');

        write_string(writer, &entry.title.to_string());
    }
}

fn handler_select_process(reader: &mut Read, writer: &mut Write, ctx: &mut Context) {
    if ctx.pid != 0 {
        write_u32(writer, 0);
        write_string(writer, "agent already attached");
        return;
    }

    debug!("entering handler_select_process");
    let pid = read_u32(reader);
    debug!("got pid: {}", pid);

    match ctx.p_enum.mem_map(pid) {
        Err(msg) => {
            debug!("mem_map error: {:?}", msg);
            write_u32(writer, 0);
            write_string(writer, msg.as_str());
            return;
        },
        Ok(maps) => {
            debug!("got maps");
            ctx.pid = pid;
            dump_maps(writer, &maps);
        }
    };
}
