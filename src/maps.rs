use std::io::{BufRead, BufReader, Read};
use MemoryRegion;
use ProtectionFlags;

macro_rules! read_until {
    ($arg1: expr, $arg2: expr) => (
        match do_read_until($arg1, $arg2) {
            Some(ret) => ret,
            None => return Err("Parse error")
        }
    )
}

macro_rules! as_number {
    ($type: ident, $str: ident) => (
        match $type::from_str_radix($str.as_str(), 16) {
            Ok(n) => n,
            Err(_) => return Err("Number conversion error")
        }
    )
}

fn do_read_until<R: Read>(rdr: &mut BufReader<R>, end_char: char) -> Option<String> {
    let mut ch: [u8; 1] = [0];
    let mut buf = String::new();

    loop {
        let len = match rdr.read(&mut ch) {
            Ok(len) => len,
            Err(_) => return None
        };

        if len != 1 {
            return None;
        }

        if ch[0] != end_char as u8 {
            buf.push(ch[0] as char);
        } else {
            break;
        }
    }

    Some(buf)
}

fn parse_line(line: String) -> Result<MemoryRegion, &'static str> {
    let mut rdr = BufReader::new(line.as_bytes());

    let begin_offset = read_until!(&mut rdr, '-');
    let end_offset = read_until!(&mut rdr, ' ');
    let prot = read_until!(&mut rdr, ' ');
    let _size = read_until!(&mut rdr, ' ');
    let _dev_major = read_until!(&mut rdr, ':');
    let _dev_minor = read_until!(&mut rdr, ' ');
    let _inode = read_until!(&mut rdr, ' ');

    let mut title = String::new();
    match rdr.read_line(&mut title) {
        Ok(_) => { },
        Err(_) => return Err("Parse error")
    };

    if prot.len() != 4 {
        return Err("prot is not 4 bytes");
    }

    let trimmed_title = title.as_str().trim();
    let prot_str = prot.as_bytes();

    Ok(MemoryRegion {
        region_begin: as_number!(u64, begin_offset),
        region_end: as_number!(u64, end_offset),

        prot: ProtectionFlags {
            readable: prot_str[0] == ('r' as u8),
            writable: prot_str[1] == ('w' as u8),
            executable: prot_str[2] == ('x' as u8),
        },

        title: trimmed_title.to_string(),
    })
}

pub fn parse_maps<R: Read>(rdr: &mut BufReader<R>) -> Result<Vec<MemoryRegion>, &'static str> {
    let mut vec: Vec<MemoryRegion> = Vec::new();

    for (_index, line) in rdr.lines().enumerate() {
        let line_str = match line {
            Ok(line) => line,
            Err(_) => return Err("I/O error")
        };

        let item = match parse_line(line_str) {
            Ok(result) => result,
            Err(msg) => return Err(msg)
        };

        vec.push(item);
    }

    Ok(vec)
}
