use ProcessEnumerator;
use super::{ProcessEntry, MemoryRegion, ProtectionFlags};
use winapi::um::*;
use winapi::shared::*;
use std::os::windows::raw::HANDLE;
use std::mem;
use std::ffi::OsString;
use std::os::windows::prelude::*;

struct WindowsProcessEnumerator {
    win_specific_dummy_value: u32,
}

macro_rules! uninitialized {
    () => { unsafe { mem::uninitialized() } }
}

macro_rules! valid_handle {
    ($var: ident) => {
        if $var as u64 == u64::max_value() {
            false
        } else {
            true
        }
    }
}

macro_rules! invalid_handle {
    ($var: ident) => {
        !valid_handle!($var)
    }
}

fn create_snapshot() -> Option<u64> {
    let handle = unsafe {
        tlhelp32::CreateToolhelp32Snapshot(tlhelp32::TH32CS_SNAPPROCESS, 0)
    };

    if valid_handle!(handle) {
        Some(handle as u64)
    } else {
        None
    }
}

fn destroy_snapshot(handle: u64) {
    unsafe {
        handleapi::CloseHandle(handle as HANDLE);
    }
}

fn get_exe_path(pid: u32) -> String {
    let mut proc_name = [0u16; 4096];
    let proc_name_sz = 2 * proc_name.len();

    let handle = unsafe {
        processthreadsapi::OpenProcess(
            winnt::PROCESS_QUERY_LIMITED_INFORMATION,
            0,
            pid)
    };

    if invalid_handle!(handle) {
        return "<no access>".to_string();
    }

    let result = unsafe {
        winbase::QueryFullProcessImageNameW(handle,
            0,
            proc_name.as_mut_ptr(),
            &mut (proc_name_sz as u32) as *mut u32)
    };

    unsafe {
        handleapi::CloseHandle(handle);
    }

    if result == 0 {
        return "<no access>".to_string();
    }

    match get_osstr(&proc_name).into_string() {
        Ok(s) => s,
        Err(_) => "<encoding error>".to_string()
    }
}

fn get_osstr(wide: &[u16]) -> OsString {
    OsString::from_wide(
        &wide[0 .. wide.iter().
                       take_while(|&&c| c > 0).count()])
}

fn get_exe_name(lpe: &tlhelp32::PROCESSENTRY32W) -> String {
    match get_osstr(&lpe.szExeFile).into_string() {
        Ok(s) => s,
        Err(_) => "<encoding error>".to_string()
    }
}

fn get_module_name(me: &tlhelp32::MODULEENTRY32W) -> String {
    match get_osstr(&me.szModule).into_string() {
        Ok(s) => s,
        Err(_) => "<encoding error>".to_string()
    }
}

fn do_list_processes(handle: u64) -> Result<Vec<ProcessEntry>, String> {
    debug!("toolhelp handle: {:?}", handle);

    let mut lpe: tlhelp32::PROCESSENTRY32W = uninitialized!();
    lpe.dwSize = mem::size_of::<tlhelp32::PROCESSENTRY32W>() as u32;
    let mut vec = Vec::<ProcessEntry>::new();

    let succ = unsafe {
        tlhelp32::Process32FirstW(handle as HANDLE, &mut lpe)
    };

    if succ == 0 {
        return Err("Process32First failed".to_string());
    }

    loop {
        match unsafe { tlhelp32::Process32NextW(handle as HANDLE, &mut lpe) } {
            0 => break,
            _ => {
                let pid = lpe.th32ProcessID;
                let title = get_exe_name(&lpe);

                vec.push(ProcessEntry {
                    pid: pid,
                    command_line: "not yet".to_string(),
                    exe_path: get_exe_path(pid),
                    title: title,
                });
            }
        };
    }

    if vec.is_empty() {
        Err("encountered zero processes".to_string())
    } else {
        Ok(vec)
    }
}

fn list_processes() -> Result<Vec<ProcessEntry>, String> {
    let handle = match create_snapshot() {
        Some(x) => x,
        None => return Err("process snapshot creation failed".to_string())
    };

    let result = do_list_processes(handle);
    destroy_snapshot(handle);
    result
}

fn get_prot_flags(spec: u32) -> ProtectionFlags {
    match spec & 0xFF {
        winnt::PAGE_EXECUTE => ProtectionFlags {
            readable: false,
            writable: false,
            executable: true
        },

        winnt::PAGE_EXECUTE_READ => ProtectionFlags {
            readable: true,
            writable: false,
            executable: true
        },

        winnt::PAGE_EXECUTE_READWRITE | winnt::PAGE_EXECUTE_WRITECOPY => ProtectionFlags {
            readable: true,
            writable: true,
            executable: true
        },

        winnt::PAGE_READONLY => ProtectionFlags {
            readable: true,
            writable: false,
            executable: false
        },

        winnt::PAGE_READWRITE | winnt::PAGE_WRITECOPY => ProtectionFlags {
            readable: true,
            writable: true,
            executable: false
        },

        _ => ProtectionFlags {
            readable: false,
            writable: false,
            executable: false
        }
    }
}

struct RegionIterator {
    handle: HANDLE,
    cur_addr: u64,
}

impl RegionIterator {
    fn new(h: HANDLE) -> RegionIterator {
        RegionIterator { handle: h, cur_addr: 0 }
    }
}

impl Iterator for RegionIterator {
    type Item = MemoryRegion;

    fn next(&mut self) -> Option<Self::Item> {
        // This is needed by VirtualQueryEx.
        let buf_size = mem::size_of::<winnt::MEMORY_BASIC_INFORMATION64>();

        // VirtualQueryEx fills this structure up completely, so no need to
        // initialize it to zero.
        let mut buf: winnt::MEMORY_BASIC_INFORMATION64 = uninitialized!();

        // Needs to be in a loop, because sometimes we'll need to retry
        // the call in current iteration.
        //
        // But normally this loop will be interrupted just after first
        // iteration, and will hit second/third iteration only sporadically.
        loop {
            let size = unsafe {
                // We're compiled in 64-bit mode, so we have to use the
                // 64-bit structure MEMORY_BASIC_INFORMATION64. But the WinAPI
                // function is defined with MEMORY_BASIC_INFORMATION struct,
                // so we need to cast it to the proper type.
                memoryapi::VirtualQueryEx(self.handle,
                    self.cur_addr as minwindef::LPCVOID,
                    mem::transmute::<winnt::PMEMORY_BASIC_INFORMATION64,
                        winnt::PMEMORY_BASIC_INFORMATION>(&mut buf),
                    buf_size)
            };

            // Update the address of next region here, so we can do
            // continues and returns later in the code.
            self.cur_addr = buf.BaseAddress + buf.RegionSize;

            // Value returned by VirtualQueryEx should be the same as its
            // third argument. If it's zero, the call fails (invalid arguments,
            // region walking is finished, etc).
            //
            // RegionSize == 0 shouldn't happen. If it happens, something
            // went wrong and we can't continue.
            if size != buf_size || buf.RegionSize == 0 {
                return None
            } else {
                // Skip entries that are not marked with MEM_COMMIT. Those
                // entries are not allocated memory regions, so they're
                // not important to us.
                if buf.State & winnt::MEM_COMMIT == 0 {
                    continue;
                }

                // Return valid memory block descriptor.
                return Some(MemoryRegion {
                    region_begin: buf.BaseAddress,
                    region_end: buf.BaseAddress + buf.RegionSize,
                    prot: get_prot_flags(buf.Protect),
                    title: "".to_string(),
                })
            }
        }
    }
}

fn query_regions(handle: HANDLE) -> Vec<MemoryRegion> {
    RegionIterator::new(handle)
        // Sanity check: allow max 1000 entries to prevent bugs in iterator
        // implementation.
        .take(1000)
        .collect::<Vec<MemoryRegion>>()
}

#[derive(Debug)]
struct ModuleInfo {
    base_addr: u64,
    size: u64,
    title: String,
}

struct ModuleIterator {
    handle: HANDLE,
    me_buf: tlhelp32::MODULEENTRY32W,
    first: bool,
}

impl ModuleIterator {
    fn new(pid: u32) -> ModuleIterator {
        let mut iter_obj = ModuleIterator {
            handle: unsafe {
                tlhelp32::CreateToolhelp32Snapshot(
                    tlhelp32::TH32CS_SNAPMODULE | tlhelp32::TH32CS_SNAPMODULE32,
                    pid)
            },

            me_buf: uninitialized!(),
            first: true,
        };

        iter_obj.me_buf.dwSize =
            mem::size_of::<tlhelp32::MODULEENTRY32W>() as u32;

        let succ = unsafe {
            tlhelp32::Module32FirstW(
                iter_obj.handle,
                &mut iter_obj.me_buf)
        };

        if succ == 0 {
            panic!("can't Module32First");
        }

        iter_obj
    }
}

impl Iterator for ModuleIterator {
    type Item = ModuleInfo;

    fn next(&mut self) -> Option<Self::Item> {
        if self.first {
            self.first = false;
            return Some(ModuleInfo {
                base_addr: self.me_buf.modBaseAddr as u64,
                size: self.me_buf.modBaseSize as u64,
                title: get_module_name(&self.me_buf),
            });
        } else {
            match unsafe {
                tlhelp32::Module32NextW(self.handle,
                    &mut self.me_buf)
            } {
                0 => None,
                _ => {
                    Some(ModuleInfo {
                        base_addr: self.me_buf.modBaseAddr as u64,
                        size: self.me_buf.modBaseSize as u64,
                        title: get_module_name(&self.me_buf),
                    })
                }
            }
        }
    }
}

impl Drop for ModuleIterator {
    fn drop(&mut self) {
        unsafe {
            handleapi::CloseHandle(self.handle);
        }
    }
}

fn query_modules(pid: u32) -> Vec<ModuleInfo> {
    ModuleIterator::new(pid).collect()
}

fn update_regions_with_module_info(regs: &mut Vec<MemoryRegion>,
                                   mods: &Vec<ModuleInfo>) {
    for ref mut region in regs {
        let mut last_addr = 0u64;
        for ref mod_info in mods {
            if region.region_begin >= mod_info.base_addr &&
               region.region_end <= (mod_info.base_addr + mod_info.size) {
                region.title = mod_info.title.clone();
            }
        }
    }
}

fn mem_map(pid: u32) -> Result<Vec<MemoryRegion>, String> {
    let handle = unsafe {
        processthreadsapi::OpenProcess(
            winnt::PROCESS_QUERY_INFORMATION,
            0,
            pid)
    };

    if invalid_handle!(handle) {
        return Err("<no access>".to_string());
    }

    let modules = query_modules(pid);
    let mut result = query_regions(handle);

    unsafe {
        handleapi::CloseHandle(handle);
    }

    if result.is_empty() {
        Err("no regions found".to_string())
    } else {
        update_regions_with_module_info(&mut result, &modules);
        Ok(result)
    }
}

fn begin_read(_ctx: &WindowsProcessEnumerator, pid: u32) -> Result<(), String> {
    Err("not implemented yet".to_string())
}

fn end_read(_ctx: &WindowsProcessEnumerator) -> Result<(), String> {
    Err("not implemented yet".to_string())
}

impl ProcessEnumerator for WindowsProcessEnumerator {
    fn name(&self) -> &'static str { "windows" }

    fn list(&self) -> Result<Vec<ProcessEntry>, String> {
        list_processes()
    }

    fn mem_map(&mut self, pid: u32) -> Result<Vec<MemoryRegion>, String> {
        mem_map(pid)
    }

    fn begin_read(&mut self, pid: u32) -> Result<(), String> {
        begin_read(self, pid)
    }

    fn end_read(&self) -> Result<(), String> {
        end_read(self)
    }

    fn read(&mut self, offset: u64, buf: &mut [u8]) -> Result<usize, String> {
        Err("not implemented yet".to_string())
    }
}

pub fn create_process_enumerator() -> Box<ProcessEnumerator + Send + Sync> {
    Box::new(WindowsProcessEnumerator {
        win_specific_dummy_value: 0,
    })
}
