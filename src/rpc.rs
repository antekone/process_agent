use capnp;
use packets_capnp::process_agent_rpc;

pub struct ProcessAgentRPCImpl {
    pub instance: i32,
}

use capnp::capability::Promise;

use packets_capnp::process_agent_rpc::{BannerParams, BannerResults, ListProcessesParams, ListProcessesResults};
type RpcReturn = Promise<(), capnp::Error>;

impl process_agent_rpc::Server for ProcessAgentRPCImpl {
    fn banner(&mut self, _: BannerParams, mut results: BannerResults) -> RpcReturn {
        let mut resp = results.get().get_resp().unwrap();
        resp.set_banner_text("init1");
        Promise::ok(())
    }

    fn list_processes(&mut self, _: ListProcessesParams, _: ListProcessesResults) -> RpcReturn {
        Promise::ok(())
    }
}

